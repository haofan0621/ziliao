#include <string.h>
#include "esp_mac.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#define ESP_WIFI_SSID     "Esp32C3"
#define ESP_WIFI_PASS      "12345678"
static const char *TAG = "softAP";

static void wifi_event_handler(void* arg, esp_event_base_t event_base,
                                    int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_AP_STACONNECTED) {
        wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" join, AID=%d", 
        MAC2STR(event->mac), event->aid);
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_AP_STADISCONNECTED) {
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" leave, AID=%d",
                 MAC2STR(event->mac), event->aid);
    }
    else if(event_base == IP_EVENT && event_id == IP_EVENT_AP_STAIPASSIGNED)
    {
        ip_event_ap_staipassigned_t* event = (ip_event_ap_staipassigned_t*)event_data;
        ESP_LOGI(TAG, "ip:"IPSTR"", IP2STR(&event->ip));
    }
}

void app_main(void)
{
    ESP_ERROR_CHECK(nvs_flash_init());

    /*1.创建系统事件循环*/   
    ESP_ERROR_CHECK(esp_event_loop_create_default()); 

    /*2.创建事件实例的回调函数*/

    /*3.将wifi的所有事件作为实例注册进系统事件循环中*/
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        NULL));

    /*4.将sta被成功分配ip的事件作为实例注册进系统事件循环中*/
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_AP_STAIPASSIGNED,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        NULL));

    /*5.初始化网卡相关的底层配置*/                                                  
    ESP_ERROR_CHECK(esp_netif_init());   

    /*6.以默认的方式创建一个ap类型的网卡*/
    esp_netif_create_default_wifi_ap();

    /*7.初始化wifi底层配置*/
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();

    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    /*8.将wifi设置成ap模式*/
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));

    /*9.配置ap的属性*/
    wifi_config_t wifi_config = {
        .ap = {
            .ssid = ESP_WIFI_SSID,
            .ssid_len = strlen(ESP_WIFI_SSID),
            .password = ESP_WIFI_PASS,
            .max_connection = 4,
            .authmode = WIFI_AUTH_WPA_WPA2_PSK
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));

    /*10.开启wifi*/
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s password:%s",
             ESP_WIFI_SSID, ESP_WIFI_PASS);
}
