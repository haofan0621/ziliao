#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "lwip/inet.h"

#define ESP_WIFI_SSID "Esp_Test"
#define ESP_WIFI_PASS "12345678"
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT BIT1

static const char *TAG = "example";
EventGroupHandle_t WIFI_EventHandle;
esp_netif_t* pSTANetif;

void event_CallBack(void *arg, esp_event_base_t event_base,
                    int32_t event_id, void *event_data)
{
    static uint8_t connectTryCount;
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (connectTryCount < 5)
        {
            esp_wifi_connect();
            connectTryCount++;
        }
        else
            xEventGroupSetBits(WIFI_EventHandle, WIFI_FAIL_BIT);
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t *event_Ip_Msg = (ip_event_got_ip_t *)event_data;
        ESP_LOGI(TAG, "已成功连接到AP\n获取到的的IP地址为:"IPSTR"", IP2STR(&event_Ip_Msg->ip_info.ip));
        ESP_LOGI(TAG, "网关的IP地址为:"IPSTR"", IP2STR(&event_Ip_Msg->ip_info.gw));
        
        esp_netif_dns_info_t dns_info;
        esp_netif_get_dns_info(event_Ip_Msg->esp_netif, ESP_NETIF_DNS_MAIN, &dns_info);
        ESP_LOGI(TAG, "主dns:"IPSTR"", IP2STR(&dns_info.ip.u_addr.ip4));
        esp_netif_get_dns_info(event_Ip_Msg->esp_netif, ESP_NETIF_DNS_BACKUP, &dns_info);
        ESP_LOGI(TAG, "备用dns:"IPSTR"", IP2STR(&dns_info.ip.u_addr.ip4));
        
        xEventGroupSetBits(WIFI_EventHandle, WIFI_CONNECTED_BIT);
    }
}

void app_main(void)
{
    ESP_ERROR_CHECK(nvs_flash_init());
    /*1.创建事件组*/
    WIFI_EventHandle = xEventGroupCreate();

    /*2.创建默认事件循环*/
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /*3.创建事件实例的回调函数*/

    /*4.把WIFI的所有事件注册进默认事件循环中*/
    esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, event_CallBack, NULL, NULL);

    /*5.把sta获取ip成功的事件注册进默认事件循环中*/
    esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP, event_CallBack, NULL, NULL);

    /*6.初始化网卡底层设置*/
    ESP_ERROR_CHECK(esp_netif_init());

    /*7.用默认的方式创建sta类型网卡*/
    pSTANetif = esp_netif_create_default_wifi_sta();

    /*8.设置静态IP*/
     if (esp_netif_dhcpc_stop(pSTANetif) != ESP_OK) {
        ESP_LOGE(TAG, "Failed to stop dhcp client");
    }  

    esp_netif_ip_info_t static_ip = {0};
    static_ip.gw.addr = inet_addr("192.168.137.1");
    static_ip.ip.addr = inet_addr("192.168.137.2");
    static_ip.netmask.addr = inet_addr("255.255.255.0");
    esp_netif_set_ip_info(pSTANetif, &static_ip);

    esp_netif_dns_info_t dns = {0};
    dns.ip.u_addr.ip4.addr = inet_addr("192.168.137.1");
    dns.ip.type = IPADDR_TYPE_V4;
    ESP_ERROR_CHECK(esp_netif_set_dns_info(pSTANetif, ESP_NETIF_DNS_MAIN, &dns));
    /*9.初始化wifi底层设置*/
    wifi_init_config_t wifi_cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_cfg));
    esp_wifi_set_ps(WIFI_PS_NONE);

    /*10.将wifi的模式设置成sta*/
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

    /*11.设置sta的属性*/
    wifi_config_t wifi_config =
    {
        .sta =
            {
                .ssid = ESP_WIFI_SSID,
                .password = ESP_WIFI_PASS,
            },
    };
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));

    /*12.启动wifi*/
    ESP_ERROR_CHECK(esp_wifi_start());

    EventBits_t event_Bits = xEventGroupWaitBits(WIFI_EventHandle, WIFI_CONNECTED_BIT | WIFI_FAIL_BIT, pdTRUE, pdFALSE, portMAX_DELAY);
    if (event_Bits & WIFI_FAIL_BIT)
    {
        ESP_LOGI(TAG, "WIFI连接失败\n");
        // 错误处理
    }
    else if (event_Bits & WIFI_CONNECTED_BIT)
    {
        ESP_LOGI(TAG, "WIFI连接成功");
    }
}
