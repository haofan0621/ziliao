#include <sys/select.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "lwip/sockets.h"

#define ESP_WIFI_SSID "Esp_Test"
#define ESP_WIFI_PASS "12345678"
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT BIT1

#define CONNECT_MAX_TRY 5

static const char *TAG = "example";
EventGroupHandle_t WIFI_EventHandle;
uint32_t Server_IP;
int SocketServer;

char Recv_Buff[1501] = {0};

fd_set Socket_Fd;
typedef struct
{
    int m_socket[FD_SETSIZE];
    uint8_t m_size;
}SOCK_SET;
SOCK_SET AllSockets;

void wifi_init_sta(void);
bool Socket_Push(int s, SOCK_SET* set);
bool Socket_Erase(int s, SOCK_SET* set);
void Socket_Close(SOCK_SET* set);

void  event_CallBack(void* arg, esp_event_base_t event_base, 
                        int32_t event_id, void* event_data)
{ 
    static uint8_t connectTryCount;
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (connectTryCount < CONNECT_MAX_TRY)
        {
            esp_wifi_connect();
            connectTryCount++;
        }
        else
            xEventGroupSetBits(WIFI_EventHandle, WIFI_FAIL_BIT);
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t *event_Ip_Msg = (ip_event_got_ip_t *)event_data;
        ESP_LOGI(TAG, "已成功连接到AP\n获取到的的IP地址为:"IPSTR"", IP2STR(&event_Ip_Msg->ip_info.ip));
        ESP_LOGI(TAG, "网关的IP地址为:"IPSTR"", IP2STR(&event_Ip_Msg->ip_info.gw));
        Server_IP = event_Ip_Msg->ip_info.ip.addr;
        xEventGroupSetBits(WIFI_EventHandle, WIFI_CONNECTED_BIT);
    }
}

void app_main(void)
{
    ESP_ERROR_CHECK(nvs_flash_init());
    wifi_init_sta();
    EventBits_t event_Bits =  xEventGroupWaitBits(WIFI_EventHandle, WIFI_CONNECTED_BIT | WIFI_FAIL_BIT, pdTRUE, pdFALSE, portMAX_DELAY);
    if (event_Bits & WIFI_FAIL_BIT)
    {
        ESP_LOGI(TAG, "WIFI连接失败\n");
        // 错误处理
        return;
    }
    else if (event_Bits & WIFI_CONNECTED_BIT)
    {
        ESP_LOGI(TAG, "WIFI连接成功");
    }

    /*1.创建服务器套接字*/
    SocketServer = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(SocketServer == -1)
    {
        ESP_LOGI(TAG, "服务器套接字创建失败");
        return;
    }

    /*2.绑定IP和端口号*/
    struct sockaddr_in Server_Msg = {0};
    Server_Msg.sin_family = AF_INET;
    Server_Msg.sin_port = htons(30000);
    Server_Msg.sin_addr.s_addr = Server_IP;
    if(0 != bind(SocketServer, (struct sockaddr*)&Server_Msg, sizeof(Server_Msg)))
    {
        ESP_LOGI(TAG, "绑定服务器套接字时出错");
        return;
    }

    /*3.开始监听*/
    if(0 != listen(SocketServer, 5))
    {
        ESP_LOGI(TAG, "监听时出错");
        return;
    }

    /*4.储存套接字，并置位其对应的文件描述符*/
    FD_ZERO(&Socket_Fd);
    FD_SET(SocketServer, &Socket_Fd);
    Socket_Push(SocketServer, &AllSockets);

    /*4.select模型开始*/
    while(1)
    {   
        fd_set Temp_Fd = Socket_Fd;
        SOCK_SET Temp_set = AllSockets;
        ESP_LOGI(TAG, "Select 开始了!当前fd_set是%lu和%lu", Temp_Fd.__fds_bits[0], Temp_Fd.__fds_bits[1]);
        int nRes = select(FD_SETSIZE, &Temp_Fd, NULL, NULL, NULL);
        if(nRes < 0) continue;
        for(int i = 0; i < Temp_set.m_size; i++)
        {
            if(FD_ISSET(Temp_set.m_socket[i], &Temp_Fd))
            {
                //如果是服务器，说明有客户端来连接了，要accept
                if(Temp_set.m_socket[i] == SocketServer)
                {
                    int socketClient = accept(SocketServer, NULL, NULL);
                    if(socketClient > 0) 
                    {
                        FD_SET(socketClient, &Socket_Fd);
                        Socket_Push(socketClient, &AllSockets);
                        ESP_LOGI(TAG, "客户端%d已接入", socketClient);
                        send(socketClient, "accept OK!", strlen("accept OK!"), 0);
                    }
                }
                //如果不是客户端，说明这个客户端发来消息了，要recv它
                else
                {
                    int recv_Count = recv(Temp_set.m_socket[i], Recv_Buff, 1500, 0);
                    if(recv_Count == -1)
                    {
                        ESP_LOGI(TAG, "客户端%d意外下线", Temp_set.m_socket[i]);
                        FD_CLR(Temp_set.m_socket[i], &Socket_Fd);
                        Socket_Erase(Temp_set.m_socket[i], &AllSockets);
                        shutdown(Temp_set.m_socket[i], 2);
                        close(Temp_set.m_socket[i]);
                    }
                    else if(recv_Count == 0)
                    {
                        ESP_LOGI(TAG, "客户端%d正常下线", Temp_set.m_socket[i]);
                        FD_CLR(Temp_set.m_socket[i], &Socket_Fd);
                        Socket_Erase(Temp_set.m_socket[i], &AllSockets);
                        shutdown(Temp_set.m_socket[i], 2);
                        close(Temp_set.m_socket[i]);
                    }
                    else
                    {
                        Recv_Buff[recv_Count] = 0;
                        ESP_LOGI(TAG, "接收到客户端%d的消息:%s", Temp_set.m_socket[i], Recv_Buff);
                        send(Temp_set.m_socket[i], "recv OK!", strlen("recv OK!"), 0);
                    }
                }
            }
        }
    }
}

void wifi_init_sta(void)
{
    /*1.创建事件组*/
    WIFI_EventHandle = xEventGroupCreate();

    /*2.创建默认事件循环*/
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /*3.创建事件实例的回调函数*/

    /*4.把WIFI的所有事件注册进默认事件循环中*/
    esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, event_CallBack, NULL, NULL);

    /*5.把sta获取ip成功的事件注册进默认事件循环中*/
    esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP, event_CallBack, NULL, NULL);

    /*6.初始化网卡底层设置*/
    ESP_ERROR_CHECK(esp_netif_init());

    /*7.用默认的方式创建sta类型网卡*/
    esp_netif_create_default_wifi_sta();

    /*8.初始化wifi底层设置*/
    wifi_init_config_t wifi_cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_cfg));
    esp_wifi_set_ps(WIFI_PS_NONE);

    /*9.将wifi的模式设置成sta*/
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

    /*10.设置sta的属性*/
    wifi_config_t wifi_config =
    {
        .sta =
            {
                .ssid = ESP_WIFI_SSID,
                .password = ESP_WIFI_PASS,
            },
    };
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));

    /*11.启动wifi*/
    ESP_ERROR_CHECK(esp_wifi_start());
}

bool Socket_Push(int s, SOCK_SET* set)
{
    if(set->m_size >= 64) return false;
    set->m_socket[set->m_size] = s;
    set->m_size++;
    return true;
}

bool Socket_Erase(int s, SOCK_SET* set)
{
    for(int i = 0; i < set->m_size; i++)
    {
        if(set->m_socket[i] == s)
        {
            set->m_socket[i] = set->m_socket[set->m_size - 1];
            set->m_size--;
            return true;
        }
    }
    return false;
}

void Socket_Close(SOCK_SET* set)
{
    for(int i = 0; i < set->m_size; i++)
    {
        shutdown(set->m_socket[i], 0);
        close(set->m_socket[i]);
    }
    set->m_size = 0;
}