#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_netif.h"
#include "lwip/sockets.h"

#define ESP_WIFI_SSID   "Esp_Test"
#define ESP_WIFI_PASS   "12345678"

#define CONNECT_MAX_TRY 5
#define WIFI_CONNECTED_BIT      BIT0
#define WIFI_FAIL_BIT           BIT1

EventGroupHandle_t WIFI_EventGroupHandle;
char Recv_Buff[513];
uint32_t Server_IP;
static const char *TAG = "example";

void wifi_init_sta(void);

void event_CallBack(void* arg, esp_event_base_t event_base, 
                        int32_t event_id, void* event_data)
{
    static uint8_t connectTryCount;
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (connectTryCount < CONNECT_MAX_TRY)
        {
            esp_wifi_connect();
            connectTryCount++;
        }
        else
            xEventGroupSetBits(WIFI_EventGroupHandle, WIFI_FAIL_BIT);
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t *event_Ip_Msg = (ip_event_got_ip_t *)event_data;
        ESP_LOGI(TAG, "已成功连接到AP\n获取到的的IP地址为:"IPSTR"", IP2STR(&event_Ip_Msg->ip_info.ip));
        ESP_LOGI(TAG, "网关的IP地址为:"IPSTR"", IP2STR(&event_Ip_Msg->ip_info.gw));
        Server_IP = event_Ip_Msg->ip_info.ip.addr;
        xEventGroupSetBits(WIFI_EventGroupHandle, WIFI_CONNECTED_BIT);
    }
}

void app_main(void)
{
    ESP_ERROR_CHECK(nvs_flash_init());
    wifi_init_sta();
    EventBits_t event_Bits =  xEventGroupWaitBits(WIFI_EventGroupHandle, WIFI_CONNECTED_BIT | WIFI_FAIL_BIT, pdTRUE, pdFALSE, portMAX_DELAY);
    if (event_Bits & WIFI_FAIL_BIT)
    {
        ESP_LOGI(TAG, "WIFI连接失败\n");
        // 错误处理
        return;
    }
    else if (event_Bits & WIFI_CONNECTED_BIT)
    {
        ESP_LOGI(TAG, "WIFI连接成功");
    }

    /*1.创建服务器套接字*/
    int socket_Server = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(socket_Server < 0)
    {
        ESP_LOGI(TAG, "套接字创建失败");
        return;
    }

    /*2.绑定*/
    struct sockaddr_in server_Msg = {0};
    server_Msg.sin_family = AF_INET;
    server_Msg.sin_port = htons(30000);
    server_Msg.sin_addr.s_addr = Server_IP;

    if(0 > bind(socket_Server, (const struct sockaddr *)&server_Msg, sizeof(server_Msg)))
    {
        ESP_LOGI(TAG, "套接字绑定失败");
        return;
    }

    /*3.开始收发消息*/
    struct sockaddr client_Msg = {0};
    uint32_t client_Size = sizeof(client_Msg);
    int recv_Count = 0;
    while(1)
    {
        recv_Count = recvfrom(socket_Server, Recv_Buff, 512, 0, &client_Msg, &client_Size);
        Recv_Buff[recv_Count] = 0;
        ESP_LOGI(TAG, "收到消息:%s", Recv_Buff);
        sendto(socket_Server, "Recv OK!", strlen("Recv OK!"), 0, &client_Msg, client_Size);
    }
}

void wifi_init_sta(void)
{
    /*1.创建事件组*/
    WIFI_EventGroupHandle = xEventGroupCreate();

    /*2.创建默认事件循环*/
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /*3.创建事件实例的回调函数*/

    /*4.把WIFI的所有事件注册进默认事件循环中*/
    esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, event_CallBack, NULL, NULL);

    /*5.把sta获取ip成功的事件注册进默认事件循环中*/
    esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP, event_CallBack, NULL, NULL);

    /*6.初始化网卡底层设置*/
    ESP_ERROR_CHECK(esp_netif_init());

    /*7.用默认的方式创建sta类型网卡*/
    esp_netif_create_default_wifi_sta();

    /*8.初始化wifi底层设置*/
    wifi_init_config_t wifi_cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_cfg));
    esp_wifi_set_ps(WIFI_PS_NONE);

    /*9.将wifi的模式设置成sta*/
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

    /*10.设置sta的属性*/
    wifi_config_t wifi_config =
    {
        .sta =
            {
                .ssid = ESP_WIFI_SSID,
                .password = ESP_WIFI_PASS,
            },
    };
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));

    /*11.启动wifi*/
    ESP_ERROR_CHECK(esp_wifi_start());
}
